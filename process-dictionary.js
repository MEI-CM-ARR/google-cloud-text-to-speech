'use strict';

// [START tts_quickstart]
// Imports the Google Cloud client library
const textToSpeech = require('@google-cloud/text-to-speech');
const fsRegular = require('fs');
const fs = fsRegular.promises;

// let voices = ['a', 'b', 'c', 'd'];
let voices = ['a'];
async function main() {

    // let dictionary = await fs.readFile('dictionary.txt');
    // dictionary = dictionary.toString().split('\n');
    let dictionary = ['A casa que os Maias vieram habitar em Lisboa, no outono de 1875, era conhecida na vizinhança da rua de S. Francisco de Paula, e em todo o bairro das Janelas Verdes, pela casa do Ramalhete ou simplesmente o Ramalhete'];
    // Creates a client
    let client = new textToSpeech.TextToSpeechClient();

    for(let voice of voices){
        for (let line of dictionary) {
            line = line.toLowerCase();
            let filename = 'audio/voice-' + voice +'/' + line + '.wav';

            try {
                await fs.access(filename, fs.F_OK);
                continue;
            }catch (e) {

            }

            // Construct the request
            const request = {
                input: {text: line},
                // Select the language and SSML Voice Gender (optional)
                voice: {languageCode: 'pt-PT', ssmlGender: 'NEUTRAL', name: 'pt-PT-Standard-' + voice},
                // Select the type of audio encoding
                audioConfig: {audioEncoding: 'LINEAR16'},
            };

            // Performs the Text-to-Speech request
            let response;
            try {
               [response] = await client.synthesizeSpeech(request);
            }catch (e) {
                client = new textToSpeech.TextToSpeechClient();
                [response] = await client.synthesizeSpeech(request);
            }

            // Write the binary audio content to a local file

            await fs.writeFile(filename, response.audioContent, 'binary');
            console.log('Saved ' + filename);
            await sleep(350);
        }

    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// [END tts_quickstart]
main().catch(console.error);