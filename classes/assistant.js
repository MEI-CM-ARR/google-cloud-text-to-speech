'use strict';

const Dictionary = require('./dictionary.js');
const TextToSpeechAPI = require('./text-to-speech-api.js');

const fsRegular = require('fs');
const fs = fsRegular.promises;

const util = require('util');
const exec = util.promisify(require('child_process').exec);
const wavFileInfo = require('wav-file-info');

class Assistant {

    constructor(){
        this.voices = ['a', 'b', 'c', 'd'];
        this.currentVoice = this.voices[0];
        this.dictionary = new Dictionary();
        this.inlearningMode = true;
    }

    async loadWordsSoundFiles(words){
        let folder = 'audio/voice-' + this.currentVoice + '/';
        let files = [];
        for (let word of words) {
            word = word.toLocaleLowerCase();
            let filename = folder + word + '.wav';
            let fileExits = true;
            try {
                await fs.access(filename, fsRegular.F_OK)
            } catch (e) {
                fileExits = false;
            }

            if (!fileExits) {
                await this.learnNewWord(word);
            }

            files.push(filename);
        }
        return files;
    }

    async learnNewWord(word){
        if (!this.inlearningMode){
            return;
        }

        let textToSpeechAPI = new TextToSpeechAPI();
        let response = await textToSpeechAPI.getWordSound(word, this.currentVoice);
        let filename = 'audio/voice-' + this.currentVoice +'/' + word + '.wav';
        await fs.writeFile(filename, response.audioContent, 'binary');
        await this.dictionary.addNewWord(word);

        return filename;
    }

    async speak(text){
        let words = text.replace('/,/g', '').replace('/./g', '').replace('/?/g', '').split(' ');
        let files = await this.loadWordsSoundFiles(words);

        for (let file of files) {
            let duration = await this.getDuration(file);
            exec('aplay ' + file);
            await this.sleep(duration * 1000 * 0.80);
        }
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }


    getDuration(file) {
        return new Promise((resolve, reject) => {
            wavFileInfo.infoByFilename(file, function (err, info) {
                if (err) {
                    reject();
                    return;
                }
                resolve(info.duration);
            });
        });

    }

}

module.exports = Assistant;