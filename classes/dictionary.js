'use strict';

const fsRegular = require('fs');
const fs = fsRegular.promises;

class Dictionary {

    constructor() {
        this.data = [];
        this.loadData();
    }

    async loadData() {
        this.data = (await fs.readFile(__dirname + '/../dictionary.txt')).toString().split('\n');
    }

    removeDupplicates() {
        let seen = {};
        this.data = this.data.filter(function (item) {
            return seen.hasOwnProperty(item) ? false : (seen[item] = true);
        });
    }

    sort() {
        this.data = this.data.sort();
    }

    async addNewWord(word){
        if (this.data[word]){
            throw new Error('This word already exists');
        }
        this.data.push(word);
        await this.sort();
        await this.save();
    }

    async save() {
        let dataToWrite = this.data.join('\n');
        await fs.writeFile( __dirname +'/../dictionary.txt', dataToWrite);
    }

}

module.exports = Dictionary;