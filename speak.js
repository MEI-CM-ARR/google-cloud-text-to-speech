const fsRegular = require('fs');
const fs = fsRegular.promises;

const util = require('util');
const exec = util.promisify(require('child_process').exec);
const wavFileInfo = require('wav-file-info');


async function speak(text, voice) {

    let words = text.replace(',').replace('.').split(' ');

    // let folder = 'audio/voice-' + voice + '/';
    let folder = 'audio/';
    let files = [];
    for (let word of words) {
        word = word.toLocaleLowerCase();
        let filename = folder + word + '.wav';
        let fileExits = true;
        try {
            await fs.access(filename, fsRegular.F_OK)
        } catch (e) {
            fileExits = false;
        }

        files.push(filename);
    }
    for (let file of files) {
        let duration = await getDuration(file);
        exec('aplay ' + file);
        await sleep(duration * 1000 * 0.60);
    }

}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


function getDuration(file) {
    return new Promise((resolve, reject) => {
        wavFileInfo.infoByFilename(file, function (err, info) {
            if (err) {
                reject();
                return;
            }
            resolve(info.duration);
        });
    });

}

(async () => {
    await speak('O dia está bonito', 'a');
})();


